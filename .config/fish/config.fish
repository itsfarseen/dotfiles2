
# --- /home/farzeen/dotcastle//common/alternative_commands.aliases --- 
alias cat=bat

# --- /home/farzeen/dotcastle//common/emacs.aliases --- 
alias emacsw='emacs'
alias emacs='emacs -nw'
alias em='emacs -nw'

# --- /home/farzeen/dotcastle//common/git.aliases --- 
alias gst='git status'
alias gcmsg='git commit -m'
alias gps='git push'
alias gpl='git pull'
alias ga='git add'
alias gaa='git add --all'
alias gd='git diff'

# --- /home/farzeen/dotcastle//common/ls.aliases --- 
alias ls='ls -v --group-directories-first --color'


# --- /home/farzeen/dotcastle//common/nvim.aliases --- 
alias vim=nvim

# --- /home/farzeen/dotcastle//common/venv.aliases --- 
alias vv='source venv/bin/activate'

