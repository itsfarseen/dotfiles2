if [ -f ~/.environment ]; then
    . ~/.environment
fi;

if [ -f ~/.aliases ]; then
    . ~/.aliases
fi;

if [ -f ~/.ghcup/env ]; then
    . ~/.ghcup/env
fi;

if [ -f ~/.profile-$(hostname) ]; then
    . ~/.profile-$(hostname)
fi;

if command -v ssh-agent; then
    eval $(ssh-agent)
fi;

# export SSH_AUTH_SOCK=~/.ssh/ssh-agent.$HOSTNAME.sock
# ssh-add -l 2>/dev/null >/dev/null
# # run ssh-agent only if it hasn't been started
# if [ $? -ge 2 ]; then
#   ssh-agent -a "$SSH_AUTH_SOCK" >/dev/null
# fi

